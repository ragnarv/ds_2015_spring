package Controller;

import Algorithm.DisplayRoad;
import Algorithm.Kalman;
import Algorithm.RoadSpeedAlgorithm;

import Data.Taxi;
import DataIO.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

public class Main {
    public static void main(String[] args){

        //Specify time interval
        LocalDateTime startDate = LocalDateTime.of(2008, Month.FEBRUARY, 1, 14, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2008, Month.FEBRUARY, 9, 17, 59, 59);

        //Read in Taxi GPS trajectories
        System.out.println("Loading trajectories...");
        List<Taxi> data = new TaxiTrjReader("D:\\test.txt").getData();

        //Read in road network
        System.out.println("Loading road network...");
        RoadDataReader rdr = new RoadDataReader("D:\\cleanCenterBeijing.osm");
        rdr.readXMLRoadData();

        //Estimate speeds
        data.forEach(Taxi::estimateSpeeds);

        //Kalman filter part
        System.out.println("Calculating kalman estimates");
        Kalman klm = new Kalman(data);
        System.out.println("MSE: "+klm.kalmanFilter(100000));

        //Perform map-matching and calculate average link speeds
        System.out.println("Calculating average link speeds...");
        RoadSpeedAlgorithm rsa = new RoadSpeedAlgorithm(rdr.getWays(), rdr.getNodes(), data, startDate, endDate);
        rsa.calcRoadSpeeds(4);

        System.out.println("Writing speeds to file...");
        new RoadSpeedWriter("D:\\speedCentralBeijing.txt",rdr.getWays()).writeData();

        System.out.println("Generating map...");
        new DisplayRoad(rdr.getWays()).genMap();
    }
}
