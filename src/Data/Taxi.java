package Data;

import Algorithm.UtilityCalculator;

import java.util.ArrayList;
import java.util.List;

public class Taxi {
    long id;
    List<TrjNode> trjNodes;
    UtilityCalculator calc = new UtilityCalculator();

    public Taxi() {
        this.trjNodes = new ArrayList<>();
    }

    public Taxi(long id){
        this.id = id;
        this.trjNodes = new ArrayList<>();
    }

    public List<TrjNode> getTrjNodes() {
        return trjNodes;
    }

    public void setTrjNodes(List<TrjNode> trjNodes) {
        this.trjNodes = trjNodes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void estimateSpeeds() {
        for (int i = 1; i < trjNodes.size(); i++) {

            if (!trjNodes.get(i).isSkipSegment()) {
                double speed = calc.calcHaversineDist(trjNodes.get(i - 1), trjNodes.get(i)) /
                        calc.calcTimeDif(trjNodes.get(i - 1),trjNodes.get(i));
                trjNodes.get(i - 1).setSpeed(speed);
                trjNodes.get(i).setSpeed(speed);
            }
        }
    }

    @Override
    public String toString() {
        return "Taxi{" +
                "id=" + id +
                ", points=" + trjNodes +
                '}';
    }
}
