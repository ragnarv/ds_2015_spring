package Data;


import java.util.ArrayList;
import java.util.List;

public class RoadNode extends AbstractNode{

    List<Way> ways = new ArrayList<>(4);

    public RoadNode(){}

    public RoadNode(double lon, double lat) {
        super(lon, lat);
    }

    public List<Way> getWays() {
        return ways;
    }

    public void setWays(List<Way> ways) {
        this.ways = ways;
    }

}
