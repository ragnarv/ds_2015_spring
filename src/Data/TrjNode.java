package Data;

public class TrjNode extends AbstractNode {
    String date;
    double speed;
    double kalmanSpeed;

    //Indicator set during preprocessing, which shows whether we should ignore
    //the connection between this observation and the previous one.
    boolean skipSegment;

    public TrjNode(String date, double lon, double lat, boolean skipSegment) {
        super(lon, lat);
        this.date = date;
        this.skipSegment = skipSegment;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getKalmanSpeed() {
        return kalmanSpeed;
    }

    public void setKalmanSpeed(double kalmanSpeed) {
        this.kalmanSpeed = kalmanSpeed;
    }

    public boolean isSkipSegment() {
        return skipSegment;
    }

    public void setSkipSegment(boolean skipSegment) {
        this.skipSegment = skipSegment;
    }

    @Override
    public String toString() {
        return "trjPoint{" +super.toString()+
                ", date='" + date + '\'' +
                '}';
    }
}
