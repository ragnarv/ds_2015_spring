package Data;

import java.util.Comparator;

public class XComparator implements Comparator<AbstractNode> {

    @Override
    public int compare(AbstractNode x, AbstractNode y) {
        if (x.getLon() < y.getLon())
            return -1;
        if (x.getLon() > y.getLon())
            return 1;
        return 0;
    }
}
