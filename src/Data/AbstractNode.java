package Data;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractNode implements Comparable<AbstractNode>{
    double lon;
    double lat;

    public AbstractNode(){
    }

    public AbstractNode(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof AbstractNode))
            return false;

        AbstractNode p = (AbstractNode) obj;
        return compareTo(p) == 0;
    }

    @Override
    public int compareTo(@NotNull AbstractNode o) {
        int xComp = new XComparator().compare(this, o);
        if (xComp != 0)
            return xComp;
        return new YComparator().compare(this, o);
    }

    @Override
    public String toString() {
        return "Point{" +
                "lon=" + lon +
                ", lat=" + lat +
                '}';
    }
}
