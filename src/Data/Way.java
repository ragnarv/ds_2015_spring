package Data;

import java.util.ArrayList;
import java.util.List;

public class Way {
    long id;
    List<RoadNode> nodes = new ArrayList<>();
    List<Double> trjAvg = new ArrayList<>();
    double wayFinalAverage;

    public List<RoadNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<RoadNode> nodes) {
        this.nodes = nodes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Double> getTrjAvg() {
        return trjAvg;
    }

    public void setTrjAvg(List<Double> trjAvg) {
        this.trjAvg = trjAvg;
    }

    public double getWayFinalAverage() {
        return wayFinalAverage;
    }

    public void setWayFinalAverage(double wayFinalAverage) {
        this.wayFinalAverage = wayFinalAverage;
    }

    @Override
    public String toString() {
        return "Link{" +
                "id=" + id +
                ", link speed " + wayFinalAverage +
                ", nodes=" + nodes +
                '}';
    }
}
