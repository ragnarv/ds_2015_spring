package Data;

import java.util.Comparator;

public class YComparator implements Comparator<AbstractNode> {

    @Override
    public int compare(AbstractNode x, AbstractNode y) {
        if (x.getLat() < y.getLat())
            return -1;
        if (x.getLat() > y.getLat())
            return 1;
        return 0;
    }
}
