package Algorithm;


import Data.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class RoadSpeedAlgorithm {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    UtilityCalculator calc = new UtilityCalculator();
    List<Way> ways;
    KdTree<AbstractNode> nodes;
    List<Taxi> taxiTrjs;
    LocalDateTime startDate;
    LocalDateTime endDate;


    public RoadSpeedAlgorithm(List<Way> ways, Map<Long, RoadNode> nodes, List<Taxi> taxiTrjs,
                              LocalDateTime startDate, LocalDateTime endDate) {
        this.ways = ways;
        this.nodes = new KdTree<>(new ArrayList<>(nodes.values()));
        this.taxiTrjs = taxiTrjs;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public void calcRoadSpeeds(int nrOfThreads) {

        int incr = taxiTrjs.size() / nrOfThreads;
        List<Thread> threads = new ArrayList<>();
        for (int k = 0; k < nrOfThreads; k++) {
            int i = k;
            if (i == nrOfThreads - 1) {
                threads.add(new Thread(() -> {
                    for (int j = i * incr; j < taxiTrjs.size(); j++) {
                        if (j == ((i * incr) + taxiTrjs.size()) / 2) {
                            System.out.println("Thread " + (i + 1) + " - 50% done");
                        }
                        calcSingleTrjRoadSpeeds(taxiTrjs.get(j).getTrjNodes());
                    }
                }));
            } else {
                threads.add(new Thread(() -> {
                    for (int j = i * incr; j < (i + 1) * incr; j++) {
                        if (j == (int)(incr * (0.5 + i))) {
                            System.out.println("Thread " + (i + 1) + " - 50% done");
                        }
                        calcSingleTrjRoadSpeeds(taxiTrjs.get(j).getTrjNodes());
                    }
                }));
            }
        }
        threads.forEach(Thread::start);
        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        ways.stream().filter(w -> w.getTrjAvg().size() > 0).forEach(w -> w.setWayFinalAverage(calc.mean(w.getTrjAvg()) * 3.6));

    }

    private void calcSingleTrjRoadSpeeds(List<TrjNode> trj) {

        int i = 1;
        while (i < trj.size()) {

            if (trjSegmentFilter(trj.get(i - 1), trj.get(i))) {
                Way a = pointToCurve(trj.get(i - 1));
                Way b = pointToCurve(trj.get(i));

                List<Double> avgs = new ArrayList<>();
                while (a == b) {

                    //Check if we have kalman predictions
                    if(trj.get(i).getKalmanSpeed() != 0){
                        avgs.add(trj.get(i).getKalmanSpeed());
                    }else{
                        avgs.add(trj.get(i - 1).getSpeed());
                    }

                    if (++i > trj.size() - 1) {
                        break;
                    }

                    if (trjSegmentFilter(trj.get(i - 1), trj.get(i))) {
                        a = b;
                        b = pointToCurve(trj.get(i));

                    } else {
                        break;
                    }
                }
                i++;

                //If we got at least 1 speed for the road
                if (avgs.size() > 0) {
                    a.getTrjAvg().add(calc.mean(avgs));
                }
            } else {
                i++;
            }
        }
    }

    private boolean trjSegmentFilter(TrjNode a, TrjNode b) {
        LocalDateTime dt1 = LocalDateTime.parse(a.getDate(), formatter);
        return !b.isSkipSegment() && startDate.isBefore(dt1) && endDate.isAfter(dt1);
    }

    //Map-matching
    private Way pointToCurve(TrjNode tNode) {

        RoadNode closest = (RoadNode) nodes.nearestNeighbourSearch(1, tNode).iterator().next();

        double minDist = Double.POSITIVE_INFINITY;
        double curDist;
        Way closestWay = null;

        for(Way w : closest.getWays()){
            curDist = getDistToWay(w, tNode);
            if(curDist < minDist){
                closestWay = w;
                minDist = curDist;
            }
        }

        return closestWay;
    }

    private double getDistToWay(Way w, TrjNode tNode) {
        double minDist = Double.POSITIVE_INFINITY;
        double curDist;

        for(int i = 1; i < w.getNodes().size(); i++){
            curDist = Math.min(calc.calcPointToLine(w.getNodes().get(i-1), w.getNodes().get(i), tNode),
                    Math.min(calc.calcHaversineDist(w.getNodes().get(i-1), tNode),
                            calc.calcHaversineDist(w.getNodes().get(i), tNode)));
            minDist = curDist < minDist ? curDist : minDist;
        }
        return minDist;
    }
}
