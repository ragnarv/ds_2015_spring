package Algorithm;

import Data.AbstractNode;
import Data.TrjNode;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;


public class UtilityCalculator {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    final double R = 6371000; //Approximate radius of earth in meters

    /**
     *Calculate cross-track distance(http://www.movable-type.co.uk/scripts/latlong.html)
     * */
    public double calcPointToLine(AbstractNode p1, AbstractNode p2, AbstractNode a){

        //Intial bearing between p1 and a
        double bearing1 = calcBearing(p1, a);

        //Intial bearing between p1 and p2
        double bearing2 = calcBearing(p1, p2);

        //Calculate distance between p1 and a
        double distanceAC = calcHaversineDist(p1, a);

        //Calculate cross-track distance and return it
        return Math.abs(Math.asin(Math.sin(distanceAC/R)*Math.sin(Math.toRadians(bearing1)-Math.toRadians(bearing2))) * R);
    }

    /**
     * Calculate initial bearing from point a to b
     */
    public double calcBearing(AbstractNode a, AbstractNode b){
        double y = Math.sin(Math.toRadians(b.getLon() - a.getLon())) * Math.cos(Math.toRadians(b.getLat()));
        double x = Math.cos(Math.toRadians(a.getLat())) * Math.sin(Math.toRadians(b.getLat())) -
                Math.sin(Math.toRadians(a.getLat())) * Math.cos(Math.toRadians(b.getLat())) * Math.cos(Math.toRadians(b.getLat() - a.getLat()));
        double bearing = Math.toDegrees(Math.atan2(y, x));
        return (bearing+360) % 360;
    }


    /**
     * Calculate the difference between two points
     * based on longitude and latitude using Haversine formula
     */
    public double calcHaversineDist(AbstractNode a, AbstractNode b){
        double dLat = Math.toRadians(b.getLat()-a.getLat());
        double dLon = Math.toRadians(b.getLon() - a.getLon());
        double temp = Math.pow(Math.sin(dLat/2), 2) +
                Math.pow(Math.sin(dLon/2), 2) *
                Math.cos(Math.toRadians(a.getLat())) *
                Math.cos(Math.toRadians(b.getLat()));

        double c = 2 * Math.atan2(Math.sqrt(temp), Math.sqrt(1-temp));

        return R * c;
    }

    public double euclideanDistance(AbstractNode p1, AbstractNode p2) {
        return Math.sqrt(Math.pow((p1.getLat() - p2.getLat()), 2) + Math.pow((p1.getLon() - p2.getLon()), 2));
    }

    /**
     * Calculates the time difference of two GPS points with
     * timestamps in seconds.
     */
    public long calcTimeDif(TrjNode a, TrjNode b){
        LocalDateTime dt1 = LocalDateTime.parse(a.getDate(), formatter);
        LocalDateTime dt2 = LocalDateTime.parse(b.getDate(), formatter);

        return ChronoUnit.SECONDS.between(dt1,dt2);
    }

    /**
     * Calculate sample set average
     * */
    public double mean(List<Double> vals){
        return vals.stream().mapToDouble(v -> v).average().getAsDouble();
    }
}
