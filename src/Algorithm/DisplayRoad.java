package Algorithm;

import Data.Way;
import eu.jacquet80.minigeo.MapWindow;
import eu.jacquet80.minigeo.Point;
import eu.jacquet80.minigeo.Segment;

import java.awt.*;
import java.util.List;


public class DisplayRoad {
    List<Way> ways;

    public DisplayRoad(List<Way> ways) {
        this.ways = ways;
    }

    public void genMap(){
        MapWindow window = new MapWindow();

        for(Way w : ways){

            Color segColor = getRoadColor(w);

            Point a = new Point(w.getNodes().get(0).getLat(),
                    w.getNodes().get(0).getLon()
                    );

            for(int i = 1; i < w.getNodes().size();i++){

                Point b = new Point(w.getNodes().get(i).getLat(),
                        w.getNodes().get(i).getLon());

                window.addSegment(new Segment(a,b,segColor));

                a = b;
            }
        }
        window.setVisible(true);
    }

    private Color getRoadColor(Way w){
        if(w.getWayFinalAverage() > 30){
            return Color.GREEN;
        }
        if(w.getWayFinalAverage() > 20){
            return Color.ORANGE;
        }
        if(w.getWayFinalAverage() > 0){
            return Color.RED;
        }
        return Color.BLACK;
    }
}
