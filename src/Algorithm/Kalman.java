package Algorithm;


import Data.Taxi;
import Data.TrjNode;
import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

import java.util.*;

public class Kalman {

    Random rnd = new Random();
    UtilityCalculator calc = new UtilityCalculator();
    List<Taxi> taxiTrjs;

    public Kalman(List<Taxi> taxiTrjs){
        this.taxiTrjs = taxiTrjs;
    }

    //Use the given trajectories to make predictions on random trajectory
    //points and compare them to the real estimates. Return overall MSE.
    public double kalmanFilter(int nrOfPredictions){
        Map<TrjNode, Integer> used = new HashMap<>();
        double squaredDif = 0;

        int i = 0;
        while(i < nrOfPredictions){

            //Get random trajectory
            List<TrjNode> t = taxiTrjs.get(rnd.nextInt(taxiTrjs.size())).getTrjNodes();

            //Pick random point index
            int pointIndex = rnd.nextInt(t.size()-1);

            if(!used.containsKey(t.get(pointIndex)) && //Make sure we haven't used it already
                    trjSegmentFilter(t.get(pointIndex), t.get(pointIndex+1))
                    ){

                double predictedSpeed = kalmanEstimate(t.get(pointIndex), t.get(pointIndex+1));
                t.get(pointIndex+1).setKalmanSpeed(predictedSpeed);

//                System.out.println("Estimated:"+t.get(pointIndex+1).getSpeed()+" Predicted:"+predictedSpeed
//                        + " Time difference:"+calc.calcTimeDif(t.get(pointIndex), t.get(pointIndex+1)));

                squaredDif += Math.pow(3.6*(predictedSpeed - t.get(pointIndex+1).getSpeed()),2);
                used.put(t.get(pointIndex),1);
                i++;
            }
        }
        return squaredDif/nrOfPredictions;
    }

    private boolean trjSegmentFilter(TrjNode a, TrjNode b) {

        return !b.isSkipSegment() &&
                a.getSpeed()!=0 &&
                b.getSpeed()!=0;
    }


    /**
     * Kalman estimate for next state
     */
    public double kalmanEstimate(TrjNode a, TrjNode b){

        //Calculate bearing between the given trajectory points
        double bearing = calc.calcBearing(a, b);

        //Project speed to velocities at different axes
        double evel = a.getSpeed() * Math.cos(bearing);
        double nvel = a.getSpeed() * Math.sin(bearing);

        //Convert geodetic coordinates to northing and easting in meters
        UTMRef aUTM = new LatLng(a.getLat(), a.getLon()).toUTMRef();
        UTMRef bUTM = new LatLng(b.getLat(), b.getLon()).toUTMRef();

        //Calculate time difference
        long dt = calc.calcTimeDif(a, b);



        //Generate noise
        double[] means = {0,0};
        double[][] covariances = {{0.1,0}, {0,0.1}};
        double [] noise = new MultivariateNormalDistribution(means, covariances).sample();

        //Prediction
        double el1 = (2*(bUTM.getEasting() - aUTM.getEasting())/dt) - evel + noise[0];
        double el2 = (2*(bUTM.getNorthing() - aUTM.getNorthing())/dt) - nvel + noise[1];

        return Math.sqrt(Math.pow(el1, 2) + Math.pow(el2, 2));
    }
}
