package DataIO;

import Data.RoadNode;
import Data.Way;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoadDataReader {

    String filePath;
    Map<Long, RoadNode> nodes = new HashMap<>();

    List<Way> ways = new ArrayList<>();

    public RoadDataReader(String filePath) {
        this.filePath = filePath;
    }

    public void readXMLRoadData(){
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new File(filePath));
            readNodes(doc);
            readWays(doc);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void readNodes(Document doc) {

        NodeList nodeList = doc.getElementsByTagName("node");

        for(int i = 0; i < nodeList.getLength(); i++){
            NamedNodeMap n = nodeList.item(i).getAttributes();

            long id = Long.parseLong(n.getNamedItem("id").getNodeValue());

            RoadNode p = new RoadNode(Double.parseDouble(n.getNamedItem("lon").getNodeValue()),
                    Double.parseDouble(n.getNamedItem("lat").getNodeValue()));

            nodes.put(id, p);
        }
    }

    private void readWays(Document doc) {
        NodeList nodeList = doc.getElementsByTagName("way");

        for(int i = 0; i < nodeList.getLength(); i++){
            Node n = nodeList.item(i);
            Node child = n.getFirstChild();

            Way road = new Way();
            road.setId(Long.parseLong(n.getAttributes().getNamedItem("id").getNodeValue()));

            while(child.getNextSibling() != null){
                if(child.getNodeName().equals("nd")){

                    RoadNode rn = nodes.get(Long.parseLong(
                            child.getAttributes().getNamedItem("ref").getNodeValue()));

                    //Add node to way
                    road.getNodes().add(rn);
                    //Add link to node
                    rn.getWays().add(road);
                }
                child = child.getNextSibling();
            }
            ways.add(road);
        }
    }

    public Map<Long, RoadNode> getNodes() {
        return nodes;
    }

    public List<Way> getWays() {
        return ways;
    }
}
