package DataIO;

import Data.TrjNode;
import Data.Taxi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class TaxiTrjReader {

    String filePath;

    public TaxiTrjReader(String filePath) {
        this.filePath = filePath;
    }

    public List<Taxi> getData() {
        List<Taxi> data = new ArrayList<>();


        try{
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            Taxi t = new Taxi();
            String[] taxiData;
            String line;

            while ((line = br.readLine()) != null) {
                taxiData = line.split(",");

                long id = Long.parseLong(taxiData[0]);
                if(t.getId() == 0L){
                    t.setId(id);
                }else if(t.getId() != id){
                    data.add(t);
                    t = new Taxi(id);
                }
                t.getTrjNodes().add(new TrjNode(
                        taxiData[1],
                        Double.parseDouble(taxiData[2]),
                        Double.parseDouble(taxiData[3]),
                        Boolean.parseBoolean(taxiData[4])
                ));
            }
            data.add(t);

            br.close();

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return data;
    }
}
