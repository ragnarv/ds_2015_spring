package DataIO;


import Data.Way;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class RoadSpeedWriter {

    String filePath;
    List<Way> ways;

    public RoadSpeedWriter(String filePath, List<Way> ways) {
        this.filePath = filePath;
        this.ways = ways;
    }

    public void writeData(){

        StringBuilder sb = new StringBuilder();
        ways.stream().filter(w -> w.getWayFinalAverage() != 0)
                .forEach( w -> sb.append(w.getWayFinalAverage()+System.getProperties().getProperty("line.separator")));

        try{

            File file = new File(filePath);

            if(!file.exists()){
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(sb.toString());
            bw.close();

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
